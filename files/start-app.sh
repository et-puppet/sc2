#! /bin/bash

`aws secretsmanager list-secrets |\
  jq -r '.SecretList[]|.Name' | while read id
  do 
    aws secretsmanager get-secret-value --profile uit-itlab --secret-id $id
  done | jq -s -r '.[]|"export " + (.Name|gsub("[^a-zA-Z0-9_]";"_")|ascii_upcase)+"="+.SecretString'` 

exec $*
