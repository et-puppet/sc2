# configure an SC2 (Stanford Cloud Controller) server

class sc2 (
  $description,
  $app_name,
  $jar_name,
  $jar_baseurl,
  $jar_hash,
  $packages,
  $tf_version,
  $tf_hash,
  $tf_baseurl = 'https://releases.hashicorp.com/terraform',
  $tf_url     =  "${tf_baseurl}/${tf_version}/terraform_${tf_version}_linux_${::architecture}.zip",
  $tf_alg     = 'sha256'
  $jar_alg    = 'sha1',
  $user       = $app_name,
  $uid        = 200,
  $md_url     = 'https://login.stanford.edu/metadata.xml',
  $md_file    = 'stanford-idp.xml',
  $unit_dir   = '/lib/systemd/system',
  $app_dir    = '/app',
  $bin_dir    = '/usr/bin',
  $work_dir   = "/var/lib/${app_name}",
  $log_dir    = "/var/log/${app_name}",
  $shell      = '/bin/bash',
  $user_shell = '/bin/false',
) {

  package { $packages:
    ensure  => latest,
  }

  archive { $app_name:
    ensure        => present,
    extract       => false,
    source        => "${jar_baseurl}/${jar_name}",
    checksum      => $jar_hash,
    checksum_type => $jar_alg,
    path          => "${app_dir}/${jar_name}",
    require       => File[$app_dir],
  }

  archive { 'metadata':
    ensure        => present,
    extract       => false,
    source        => $md_url,
    path          => "${app_dir}/${md_file}",
    require       => File[$app_dir],
  }

  archive { 'terraform':
    ensure        => present,
    extract       => true,
    source        => $tf_url,
    checksum      => $tf_hash,
    checksum_type => $tf_alg,
    path          => "${bin_dir}/terraform",
    require       => File[$bin_dir],
  }

  group { $user:
    ensure    => present,
    allowdupe => true,
    gid       => $uid,
    system    => true,
  }

  user { $user:
    ensure     => present,
    system     => true,
    uid        => $uid,
    comment    => $user,
    shell      => $user_shell,
    home       => "/home/${user}",
    managehome => true,
    require    => Group[$user],
  }

  file {
    [
      $work_dir,
      $log_dir
    ]:
    ensure  => directory,
    owner   => $user,
    group   => $user,
    mode    => '0755',
    require => User[$user],
  }

  file {
    [
      $app_dir,
      $bin_dir
    ]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { "${work_dir}/logs":
    ensure  => link,
    target  => $log_dir,
    owner   => $uid,
    group   => $uid,
    mode    => '0755',
    require => File[$work_dir, $log_dir],
  }

  file { "${unit_dir}/${app_name}.service":
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => template("${module_name}/app.service.erb"),
  }

  service { 'sc2':
    enable   => true,
    provider => 'systemd',
    require  => File["${unit_dir}/${app_name}.service"],
  }
}

